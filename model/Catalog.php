<?php
/**
 * @return JSON formatted tours or tour   
 **/
 
class Catalog {
public $xml;

 function Catalog(){
  $this->parseCatalogFile(); 
 }
  
  function parseCatalogFile(){
    $location = $_SERVER['DOCUMENT_ROOT'].'/catalog.xml';
    $catalogXML = file_get_contents($location,"r");
    if ($catalogXML === FALSE) throw new Exception("no catalog file");
    
    /* debug
    var_dump(extension_loaded('SimpleXML'));
    die;
    */
    
    $this->xml = new SimpleXMLElement($catalogXML,NULL,false);
  
  }
  
  // returns tour by its ID
  function getById($id){
    if (isset($this->xml->tour[$id-1])) {
    $res = $this->XML2JSON($this->xml->tour[$id-1]);
     print_r($res);
    } else throw new Exception('craft id attempt');
    
  }
  
  // returns whole catalog (all tours)
  function getCatalog(){  
    $res = $this->XML2JSON($this->xml);
    echo ($res);
   }
  
  
  // parse XML to JSON format 
  function XML2JSON($xml) {

        function normalizeSimpleXML($obj, &$result) {
            $data = $obj;
            if (is_object($data)) {
                $data = get_object_vars($data);
            }
            if (is_array($data)) {
                foreach ($data as $key => $value) {
                    $res = null;
                    normalizeSimpleXML($value, $res);
                    if (($key == '@attributes') && ($key)) {
                        $result = $res;
                    } else {
                        $result[$key] = $res;
                    }
                }
            } else {
                $result = $data;
            }
        }
        normalizeSimpleXML($xml, $result);
        return json_encode($result);
    } 
  
  

   


}

?>

