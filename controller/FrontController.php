<?php
/**
* provides routing in application 
 **/
 
class FrontController {
private $uri;
private $PAGE_LIMIT = 10;


  function FrontController($url){
    $parsed_url_arr = parse_url($url);
    
    
    // USAGE
    if ($parsed_url_arr['path'] == '/'){
      echo ('Welcome to Kalgan solutions tours JSON API.<br/>');
      echo ('USAGE urls: <br/>');
      echo ('           /katalog <br/>');
      echo ('           /detail?id=X <br/>');
      exit;
      
    }
    
    // KATALOG
    if ($parsed_url_arr['path'] == '/katalog'){
      header('Access-Control-Allow-Origin: *');
      $Catalog = new Catalog();
      $Catalog->getCatalog();
      exit;
    }
    
    // DETAIL
    if ($parsed_url_arr['path'] == '/detail'){
      header('Access-Control-Allow-Origin: *');
      
      $Catalog = new Catalog();
      
     // url contains query ?
      if(isset($parsed_url_arr['query'])){
        
        parse_str($parsed_url_arr['query'],$result);
        
        // query contains nonempty id parameter
        if (isset($result['id']) && !empty($result['id'])){
          $Catalog->getById(intval($result['id']));
          exit;
        }
          
      }
    }     
    
    
    // redirecting on USAGE
    header("HTTP/1.0 404 Not Found");
    header("Location:".$parsed_url_arr['scheme']."://".$_SERVER['HTTP_HOST']."/");

    exit; 
  }
}

?>

